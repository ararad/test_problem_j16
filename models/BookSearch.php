<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use app\models\Book;

/**
 * BookSearch represents the model behind the search form about `app\models\Book`.
 */
class BookSearch extends Book
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author_id'], 'integer'],
            [['name', 'date_create', 'date_update', 'preview', 'date', 'date_from', 'date_to', 'authors.lastname'], 'safe'],
        ];
    }
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['date_from', 'date_to', 'authors.lastname']);
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->joinWith(['author']);
        $sort = new Sort([
            'attributes' => [
                'id',
                'name',
                'preview',
                'authors.lastname',
                'date',
                'date_update',
            ],
        ]);
        $dataProvider->setSort($sort);
        $dataProvider->pagination->setPageSize(5);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
            
        $query->andFilterWhere([
            'author_id' => $this->author_id,
        ]);
        if ($this->date_from || $this->date_to) {
            if ($this->date_from) {
                $dateFrom = date('Y-m-d', strtotime($this->date_from));
            } else {
                $dateFrom = '0001-01-01';
            }
            if ($this->date_to) {
                $dateTo = date('Y-m-d', strtotime($this->date_to));
            } else {
                $dateTo = '9999-12-12';
            }
            $query->andFilterWhere(['between', 'date', $dateFrom, $dateTo]);
        }
        

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
