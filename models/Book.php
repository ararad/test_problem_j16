<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "books".
 *
 * @property integer $id
 * @property string $name
 * @property string $date_create
 * @property string $date_update
 * @property string $preview
 * @property string $date
 * @property integer $author_id
 *
 * @property Authors $author
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_create', 'date_update', 'date', 'author_id'], 'required'],
            [['date_create', 'date_update', 'date'], 'safe'],
            [['author_id'], 'integer'],
            [['preview'], 'file'],
            [['name'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'preview' => Yii::t('app', 'Preview'),
            'date' => Yii::t('app', 'Date'),
            'author_id' => Yii::t('app', 'Author ID'),
            'authors.lastname' => Yii::t('app', 'Authors Lastname'),
            'date_from' => Yii::t('app', 'Date From'),
            'date_to' => Yii::t('app', 'Date To'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Author::className(), ['id' => 'author_id']);
    }

    /**
     * @return Boolean
     */
    public function saveWithPreview($originalPreview = false)
    {
        $this->date_update = date('Y-m-d');
        if ($this->isNewRecord) {
            $this->date_create = date('Y-m-d');
        }

        $upFile = UploadedFile::getInstance($this, 'preview');
        $upFilePath = 'uploads/' . $upFile->baseName . '.' . $upFile->extension;
        if ($upFile) {
            $this->preview = $upFilePath;
        } else {
            $this->preview = $originalPreview;
        }
        $saved = $this->save();
        if ($saved && $upFile) {
            $upFile->saveAs($upFilePath);
        }
        return $saved;
    }

    public function fillDateCreate()
    {
        $this->date_create = date('Y-m-d');
    }

    public function fillDateUpdate()
    {
        $this->date_update = date('Y-m-d');
    }

    /**
     * @inheritdoc
     * @return BookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BookQuery(get_called_class());
    }
}
