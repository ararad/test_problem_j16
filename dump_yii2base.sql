-- phpMyAdmin SQL Dump
-- version 4.1.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2016 at 03:27 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `yii2basic`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE IF NOT EXISTS `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(256) NOT NULL,
  `lastname` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `firstname`, `lastname`) VALUES
(1, 'Федрмихалыч', 'Достоевский'),
(2, 'Мартин', 'Фаулер'),
(3, 'Gang of', 'Four'),
(4, 'Джеф', 'Раскин');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `date_create` date NOT NULL,
  `date_update` date NOT NULL,
  `preview` varchar(256) NOT NULL,
  `date` date NOT NULL,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`(255),`date`,`author_id`),
  KEY `fk_author_id` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `name`, `date_create`, `date_update`, `preview`, `date`, `author_id`) VALUES
(1, 'Наступление и приказание', '2015-08-08', '2016-01-04', 'uploads/images-18.jpeg', '1866-12-12', 1),
(4, 'Приёмы объектно-ориентированного проектирования. Паттерны проектирования', '2016-01-05', '2016-01-05', 'uploads/Unknown-6.jpg', '2015-08-09', 3),
(5, 'Рефакторинг: Улучшение существющего кода.', '2016-01-05', '1998-01-01', 'uploads/refactoring.jpg', '1999-01-01', 2),
(6, 'Интерфейс с человеческим лицом', '2016-01-05', '2016-01-05', 'uploads/The_Humane_Interface_book_cover.jpg', '2000-02-02', 4),
(7, 'UML. Основы. Краткое руководство по унифицированному языку моделирования', '2016-01-05', '2016-01-05', 'uploads/0.png', '2002-02-02', 2),
(8, 'Экстремальное программирование: планировани', '2016-01-05', '2016-01-05', 'uploads/da83e2797f3a.jpg', '2004-04-04', 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `fk_author_id` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`);
