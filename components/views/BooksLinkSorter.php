<?php
use yii\helpers\Html;
?>
<tr>
<?
foreach ($this->context->links as $link) {
  echo Html::tag('th', $link, $this->context->options);
}
?>
<th colspan="3"><?=Yii::t('app', 'Actions')?></th>
</tr>