<?php
namespace app\components;

use yii\widgets\LinkSorter;
use yii\helpers\Html;

class BooksLinkSorter extends LinkSorter
{
    public $links;

    /**
     * Renders the sort links.
     * @return string the rendering result
     */
    protected function renderSortLinks()
    {
        $attributes = empty($this->attributes) ? array_keys($this->sort->attributes) : $this->attributes;
        $this->links = [];
        foreach ($attributes as $name) {
            $this->links[] = $this->sort->link($name, $this->linkOptions);
        }
        return $this->render('BooksLinkSorter');
        // return Html::ul($links, array_merge($this->options, ['encode' => false]));
    }
}
