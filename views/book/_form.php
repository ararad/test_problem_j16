<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Author;
/* @var $this yii\web\View */
/* @var $model app\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'preview')->fileInput() ?>
    <?php if ($model->preview) {echo Html::img('@web/' . $model->preview, ['alt' => $model->name, 'height' => '150', 'style' => 'margin-left: 185px; margin-bottom: 30px;']);} ?>
    <?= $form->field($model, 'date')->textInput(['class' => "datepickerClass"]) ?>
    <div class="form-group field-book-author">
        <?= Html::label(Yii::t('app', 'Author'))?><?= Html::activeDropDownList($model, 'author_id', ArrayHelper::map(Author::find()->all(), 'id', 'name'), array('prompt'=>Yii::t('app', '-- Author --'), 'class' => 'form-control bookAuthor')) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>