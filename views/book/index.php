<?php

use yii\helpers\Html;
// use yii\widgets\ListView;
use yii\web\View;
use app\components\BooksListView;



/* @var $this yii\web\View */
/* @var $searchModel app\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Books');
$this->registerJs("$('.fancybox').fancybox();", View::POS_READY);
$this->registerJs("$('.datepickerClass').datepicker({dateFormat: 'yy-mm-dd'});", View::POS_READY);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="float: right;">
        <?= Html::a(Yii::t('app', 'Create Book'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <table cellpadding=5 cellspacing=5 border=1 width="100%">
    <?= BooksListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{sorter}<br>{items}<br>{pager}',
        'itemOptions' => ['class' => 'item'],
        'itemView' => 'listItem',
    ]) ?>
    </table>
</div>
