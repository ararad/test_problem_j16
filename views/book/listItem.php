<?php
use yii\helpers\Html;
?>
<tr>
	<td>
		<?= Html::encode($model->id) ?>
	</td>
	<td>
		<?= Html::encode($model->name) ?>
	</td>
	<td>
		<?= Html::a(Html::img('@web/' . $model->preview, ['alt' => $model->name, 'height' => '100']), '@web/' . $model->preview, ['class' => 'fancybox']) ?>
	</td>
	<td>
		<?= Html::encode($model->author->name) ?>
	</td>
	<td>
		<?= Html::encode($model->date) ?>
	</td>
	<td>
		<?= Html::encode($model->date_update) ?>
	</td>
	<td>
		<?= Html::a(Yii::t('app', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
	</td>
	<td>
		<?= Html::a(Yii::t('app', 'View'), ['view', 'id' => $model->id], ['class' => 'btn btn-primary fancybox fancybox.ajax'])?>
	</td>
	<td>
		<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
	</td>
</tr>

