<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Author;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BookSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>  
    <div class="form-group field-booksearch-author">
    
    <?= Html::activeDropDownList($model, 'author_id', ArrayHelper::map(Author::find()->all(), 'id', 'name'), array('prompt'=>Yii::t('app', '-- Author --'), 'class' => 'form-control bookAuthor')) ?>
    </div>

    <?= $form->field($model, 'name')->textInput(['value' => $model->name, 'class'=>'bookName']) ?>
<br/>
    <?= $form->field($model, 'date_from')->textInput(['value' => $model->date_from, 'class' => "datepickerClass"]) ?>

    <?= $form->field($model, 'date_to')->textInput(['value' => $model->date_to, 'class' => "datepickerClass"]) ?>

    

    

    <?php ActiveForm::end(); ?>

</div>
